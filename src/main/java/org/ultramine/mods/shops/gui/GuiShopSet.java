package org.ultramine.mods.shops.gui;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.Minecraft;
import net.minecraft.util.ResourceLocation;
import org.ultramine.gui.ElementButton;
import org.ultramine.gui.ElementButtonFixable;
import org.ultramine.gui.ElementLabel;
import org.ultramine.gui.ElementTextField;
import org.ultramine.gui.GuiStyled;
import org.ultramine.gui.IGuiElement;
import org.ultramine.mods.shops.te.TileEntityAdminShop;
import org.ultramine.mods.shops.te.TileEntityShop;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashSet;
import java.util.Set;

import static org.ultramine.util.I18n.tlt;

@SideOnly(Side.CLIENT)
public class GuiShopSet extends GuiStyled
{
	private static final ResourceLocation texture = new ResourceLocation("umshops:textures/gui/shopset.png");
	private static final NumberFormat dformat = new DecimalFormat("#0.##");
	
	private final TileEntityShop shop;
	private final TileEntityAdminShop admShop;
	
	private final IElementSet[] elementsSets = new IElementSet[3];
	private IElementSet currentSet;
	private byte curMode;
	
	public ElementTextField tbTradeBalance;
	public ElementTextField tbProductAmount;
	
	public GuiShopSet(TileEntityShop shop)
	{
		this.shop = shop;
		this.admShop = shop instanceof TileEntityAdminShop ? (TileEntityAdminShop)shop : null;
		setSize(256, 166);
		setBG(texture);
		
		String prodName = shop.getProduct() != null ? shop.getProduct().getItem().getItemStackDisplayName(shop.getProduct()) : tlt("ultramine.shop.guibuy.none");
		
		addElement(new ElementLabel(10, 6, tlt("ultramine.shop.guiset.name")+" (" + tlt("ultramine.shop.guiset.yousell")+" \u00a7c"+prodName+"\u00a7r)"));
		addElement(new ElementButton(0, 241, 6, 8, 8, "X"));
		addElement(new ElementButton(1, 209, 6, 30, 8, tlt("ultramine.shop.gui.back")));
		
		ElementButtonFixable mode1 = new ElementButtonFixable(2, 7, 18, 80, 12, "цена/количество");
		ElementButtonFixable mode2 = new ElementButtonFixable(3, 88, 18, 80, 12, "баланс");
		ElementButtonFixable mode3 = new ElementButtonFixable(4, 169, 18, 80, 12, "баланс цен");
		mode1.linkTo(mode2);
		mode2.linkTo(mode3);
		addElement(mode1);
		addElement(mode2);
		addElement(mode3);
		
		switchMode(shop.mode);
		switch(shop.mode)
		{
		case 0: mode1.setPressed(); break;
		case 1: mode2.setPressed(); break;
		case 2: mode3.setPressed(); break;
		}
		
		if(admShop != null)
		{
			tbTradeBalance		= new ElementTextField(-1, 200, 120, 50, 15);
			tbProductAmount		= new ElementTextField(-1, 200, 140, 50, 15);
			
			tbTradeBalance.setText(dformat.format(admShop.tradeBalance).replace(',', '.'));
			tbProductAmount.setText(Integer.toString(shop.productAmount));
			
			tbTradeBalance.setMaxStringLength(9);
			tbProductAmount.setMaxStringLength(9);
			
			tbTradeBalance.setFilterString("0123456789.");
			tbProductAmount.setFilterString("0123456789.");
			
			addElement(tbTradeBalance);
			addElement(tbProductAmount);
			
			String s1 = tlt("ultramine.shop.guiset.admin.tradebalance");
			String s2 = tlt("ultramine.shop.guiset.admin.productamount");
			addElement(new ElementLabel(195 - mc.fontRenderer.getStringWidth(s1), 123, s1));
			addElement(new ElementLabel(195 - mc.fontRenderer.getStringWidth(s2), 143, s2));
		}
	}
	
	private void switchMode(int mode)
	{
		if(currentSet != null)
			removeElements(currentSet.getElements());
		curMode = (byte)mode;
		currentSet = getElementSet(curMode);
		addElements(currentSet.getElements());
	}

	@Override
	public void actionPerformed(int id, IGuiElement el, Object... data)
	{
		switch(id)
		{
		case 0:
			Minecraft.getMinecraft().thePlayer.closeScreen();
			return;

		case 1:
			shop.sendGuiCommand(2);
			onGuiClosed();
			return;
		default:
			switchMode(id-2);
		}
	}

	@Override
	public void onGuiClosed()
	{
		if(currentSet != null)
		{
			shop.mode = curMode;
			currentSet.onGuiClosed();
		}
		
		if(admShop != null)
		{
			double tb = tbTradeBalance.getAsDoubleOr(admShop.tradeBalance);
			if(Math.abs(tb - admShop.tradeBalance) >= 0.01)
				admShop.tradeBalance = tb;
			admShop.productAmount = tbProductAmount.getAsIntegerOr(admShop.productAmount);
		}
		
		shop.sendNewShopDataToServer();
	}

	@Override
	protected void drawForeground(int mx, int my)
	{
		
	}
	
	private IElementSet getElementSet(int type)
	{
		IElementSet elementsSet = elementsSets[type];
		if(elementsSet != null)
			return elementsSet;
		switch(type)
		{
		case 0:
			elementsSet = new ElementSetDefault();
			break;
		case 1:
			elementsSet = new ElementSetBalance();
			break;
		case 2:
			elementsSet = new ElementSetBalancePrice();
			break;
		}
		
		elementsSets[type] = elementsSet;
		return elementsSet;
	}
	
	private static interface IElementSet
	{
		Set<IGuiElement> getElements();
		
		void onGuiClosed();
	}
	
	private class ElementSetDefault implements IElementSet
	{
		private final Set<IGuiElement> elements = new HashSet<IGuiElement>();
		
		public final ElementTextField tbSellPrice;
		public final ElementTextField tbBuyPrice;
		public final ElementTextField tbSellAmount;
		public final ElementTextField tbBuyAmount;
		
		public ElementSetDefault()
		{
			tbSellPrice		= new ElementTextField(-1, 10, 40, 50, 15);
			tbBuyPrice		= new ElementTextField(-1, 10, 65, 50, 15);
			tbSellAmount	= new ElementTextField(-1, 10, 90, 50, 15);
			tbBuyAmount		= new ElementTextField(-1, 10, 115, 50, 15);
			
			tbSellPrice.setText(dformat.format(shop.sellPrice).replace(',', '.'));
			tbBuyPrice.setText(dformat.format(shop.buyPrice).replace(',', '.'));
			tbSellAmount.setText(Integer.toString(shop.sellAmount));
			tbBuyAmount.setText(Integer.toString(shop.buyAmount));
			
			tbSellPrice.setMaxStringLength(9);
			tbBuyPrice.setMaxStringLength(9);
			tbSellAmount.setMaxStringLength(9);
			tbBuyAmount.setMaxStringLength(9);
			
			tbSellPrice.setFilterString("0123456789.");
			tbBuyPrice.setFilterString("0123456789.");
			tbSellAmount.setFilterString("0123456789-");
			tbBuyAmount.setFilterString("0123456789-");
			
			elements.add(tbSellPrice);
			elements.add(tbBuyPrice);
			elements.add(tbSellAmount);
			elements.add(tbBuyAmount);
			
			elements.add(new ElementLabel(12, 30, tlt("ultramine.shop.guiset.selllabel")));
			elements.add(new ElementLabel(12, 55, tlt("ultramine.shop.guiset.buylabel")));
			elements.add(new ElementLabel(12, 80, tlt("ultramine.shop.guiset.sellcount")));
			elements.add(new ElementLabel(12, 105, tlt("ultramine.shop.guiset.buycount")));
			elements.add(new ElementLabel(12, 130, tlt("ultramine.shop.guiset.footnote")));
		}
		
		@Override
		public Set<IGuiElement> getElements()
		{
			return elements;
		}

		@Override
		public void onGuiClosed()
		{
			shop.sellPrice = tbSellPrice.getAsDoubleOr(0);
			shop.buyPrice = tbBuyPrice.getAsDoubleOr(0);
			shop.sellAmount = tbSellAmount.getAsIntegerOr(0);
			shop.buyAmount = tbBuyAmount.getAsIntegerOr(0);
			
			if(shop.sellPrice < 0.01) shop.sellPrice = 0;
			if(shop.buyPrice < 0.01) shop.buyPrice = 0;
			if(shop.sellAmount < -1) shop.sellAmount = -1;
			if(shop.buyAmount < -1) shop.buyAmount = -1;
		}
	}
	
	private class ElementSetBalance implements IElementSet
	{
		private final Set<IGuiElement> elements = new HashSet<IGuiElement>();
		
		public final ElementTextField tbSellPrice;
		public final ElementTextField tbBuyPrice;
		public final ElementTextField tbBalancedAmount;
		
		public ElementSetBalance()
		{
			tbSellPrice		= new ElementTextField(-1, 10, 40, 50, 15);
			tbBuyPrice		= new ElementTextField(-1, 10, 65, 50, 15);
			tbBalancedAmount	= new ElementTextField(-1, 10, 90, 50, 15);
			
			tbSellPrice.setText(dformat.format(shop.sellPrice).replace(',', '.'));
			tbBuyPrice.setText(dformat.format(shop.buyPrice).replace(',', '.'));
			tbBalancedAmount.setText(Integer.toString(shop.sellAmount > 0 ? shop.sellAmount : 3456));
			
			tbSellPrice.setMaxStringLength(9);
			tbBuyPrice.setMaxStringLength(9);
			tbBalancedAmount.setMaxStringLength(9);
			
			tbSellPrice.setFilterString("0123456789.");
			tbBuyPrice.setFilterString("0123456789.");
			tbBalancedAmount.setFilterString("0123456789");
			
			elements.add(tbSellPrice);
			elements.add(tbBuyPrice);
			elements.add(tbBalancedAmount);
			
			elements.add(new ElementLabel(12, 30, tlt("ultramine.shop.guiset.selllabel")));
			elements.add(new ElementLabel(12, 55, tlt("ultramine.shop.guiset.buylabel")));
			elements.add(new ElementLabel(12, 80, tlt("ultramine.shop.guiset.balance.count")));
		}
		
		@Override
		public Set<IGuiElement> getElements()
		{
			return elements;
		}

		@Override
		public void onGuiClosed()
		{
			shop.sellPrice = tbSellPrice.getAsDoubleOr(0);
			shop.buyPrice = tbBuyPrice.getAsDoubleOr(0);
			shop.sellAmount = tbBalancedAmount.getAsIntegerOr(0);
			
			if(shop.sellPrice < 0.01) shop.sellPrice = 0;
			if(shop.buyPrice < 0.01) shop.buyPrice = 0;
			if(shop.sellAmount < 0) shop.sellAmount = 0;
		}
	}
	
	private class ElementSetBalancePrice implements IElementSet
	{
		private final Set<IGuiElement> elements = new HashSet<IGuiElement>();
		
		public final ElementTextField tbMinSellPrice;
		public final ElementTextField tbMaxSellPrice;
		public final ElementTextField tbMinBuyPrice;
		public final ElementTextField tbMaxBuyPrice;
		public final ElementTextField tbBalancedAmount;
		
		public ElementSetBalancePrice()
		{
			tbMinSellPrice		= new ElementTextField(-1, 10, 40, 50, 15);
			tbMaxSellPrice		= new ElementTextField(-1, 10, 65, 50, 15);
			tbMinBuyPrice		= new ElementTextField(-1, 10, 90, 50, 15);
			tbMaxBuyPrice		= new ElementTextField(-1, 10, 115, 50, 15);
			tbBalancedAmount	= new ElementTextField(-1, 10, 140, 50, 15);
			
			tbMinSellPrice.setText(dformat.format(shop.sellPrice).replace(',', '.'));
			tbMaxSellPrice.setText(dformat.format(shop.maxSellPrice).replace(',', '.'));
			tbMinBuyPrice.setText(dformat.format(shop.buyPrice).replace(',', '.'));
			tbMaxBuyPrice.setText(dformat.format(shop.maxBuyPrice).replace(',', '.'));
			tbBalancedAmount.setText(Integer.toString(shop.sellAmount > 0 ? shop.sellAmount : 3456));
			
			tbMinSellPrice.setMaxStringLength(9);
			tbMaxSellPrice.setMaxStringLength(9);
			tbMinBuyPrice.setMaxStringLength(9);
			tbMaxBuyPrice.setMaxStringLength(9);
			tbBalancedAmount.setMaxStringLength(9);
			
			tbMinSellPrice.setFilterString("0123456789.");
			tbMaxSellPrice.setFilterString("0123456789.");
			tbMinBuyPrice.setFilterString("0123456789.");
			tbMaxBuyPrice.setFilterString("0123456789.");
			tbBalancedAmount.setFilterString("123456789");
			
			elements.add(tbMinSellPrice);
			elements.add(tbMaxSellPrice);
			elements.add(tbMinBuyPrice);
			elements.add(tbMaxBuyPrice);
			elements.add(tbBalancedAmount);
			
			elements.add(new ElementLabel(12, 30, "Минимальная цена продажи"));
			elements.add(new ElementLabel(12, 55, "Максимальная цена продажи"));
			elements.add(new ElementLabel(12, 80, "Минимальная цена покупки"));
			elements.add(new ElementLabel(12, 105, "Максимальная цена покупки"));
			elements.add(new ElementLabel(12, 130, tlt("ultramine.shop.guiset.balance.count")));
		}
		
		@Override
		public Set<IGuiElement> getElements()
		{
			return elements;
		}

		@Override
		public void onGuiClosed()
		{
			shop.sellPrice = tbMinSellPrice.getAsDoubleOr(0);
			shop.buyPrice = tbMinBuyPrice.getAsDoubleOr(0);
			shop.maxSellPrice = tbMaxSellPrice.getAsDoubleOr(0);
			shop.maxBuyPrice = tbMaxBuyPrice.getAsDoubleOr(0);
			shop.sellAmount = tbBalancedAmount.getAsIntegerOr(1);
			
			if(shop.sellPrice < 0.01) shop.sellPrice = 0;
			if(shop.buyPrice < 0.01) shop.buyPrice = 0;
			if(shop.maxSellPrice < 0.01) shop.maxSellPrice = 0;
			if(shop.maxBuyPrice < 0.01) shop.maxBuyPrice = 0;
			if(shop.sellAmount < 0) shop.sellAmount = 0;
			
			if(shop.sellPrice >= shop.maxSellPrice) shop.sellPrice = shop.maxSellPrice = 0;
			if(shop.buyPrice >= shop.maxBuyPrice) shop.buyPrice = shop.maxBuyPrice = 0;
		}
	}
}

