package org.ultramine.mods.shops.packets;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.PacketBuffer;
import org.ultramine.mods.shops.economy.ClientEconomy;
import org.ultramine.network.UMPacket;

import java.io.IOException;

public class PacketPlayerMoney extends UMPacket
{
	public double moneyGSC;
	public double moneyRUR;
	
	public PacketPlayerMoney(){}
	public PacketPlayerMoney(double moneyGSC, double moneyRUR)
	{
		this.moneyGSC = moneyGSC;
		this.moneyRUR = moneyRUR;
	}

	@Override
	public void read(PacketBuffer data) throws IOException
	{
		moneyGSC = data.readDouble();
		moneyRUR = data.readDouble();
	}

	@Override
	public void write(PacketBuffer data) throws IOException
	{
		data.writeDouble(moneyGSC);
		data.writeDouble(moneyRUR);
	}

	@SideOnly(Side.CLIENT)
	public void processClient(NetHandlerPlayClient net)
	{
		ClientEconomy.GSC.setMoney(moneyGSC);
		ClientEconomy.RUR.setMoney(moneyRUR);
	}
}
