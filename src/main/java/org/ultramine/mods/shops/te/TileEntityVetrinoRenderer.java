package org.ultramine.mods.shops.te;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.IIcon;
import net.minecraft.util.MathHelper;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;

@SideOnly(Side.CLIENT)
public class TileEntityVetrinoRenderer extends TileEntitySpecialRenderer
{
	private RenderBlocks blockrender;
	private float yOffset;
	public static float f1;
	
	public TileEntityVetrinoRenderer(float yoffset)
	{
		this.yOffset = yoffset;
//		if(Config.UpDownItems)
			yOffset += 0.065F;
		blockrender = new RenderBlocks();
	}
	public void renderTileEntityAt(TileEntity tileentity, double d, double d1, double d2, float f)
	{
		ItemStack itemstack = ((TileEntityShop)tileentity).getProduct();
		renderTileEntity(itemstack, d, d1, d2, f);
	}
	public void renderTileEntity(ItemStack itemstack, double d, double d1, double d2, float f)
	{
		GL11.glPushMatrix();
		GL11.glTranslatef((float)d + 0.5F, (float)d1 + 0.8F, (float)d2 + 0.5F);
		GL11.glRotatef(0.0F, 0.0F, 1.0F, 0.0F);
		GL11.glEnable(GL12.GL_RESCALE_NORMAL);
//		if(Config.UpDownItems)
			GL11.glTranslatef(0.0F, yOffset + MathHelper.sin(f1)/10, 0.0F);
//		else
//			GL11.glTranslatef(0.0F, yOffset, 0.0F);
 
		if (itemstack != null && itemstack.getItem() != null)
		{
			TextureManager texturemanager = Minecraft.getMinecraft().getTextureManager();
			if (itemstack.getItemSpriteNumber() == 0)
			{
				texturemanager.bindTexture(texturemanager.getResourceLocation(0));
			}
			else
			{
				texturemanager.bindTexture(texturemanager.getResourceLocation(itemstack.getItemSpriteNumber()));
			}
			
			if (itemstack.getItemSpriteNumber() == 0 && itemstack.getItem() instanceof ItemBlock && RenderBlocks.renderItemIn3d(Block.getBlockFromItem(itemstack.getItem()).getRenderType()))
			{
				Block block = Block.getBlockFromItem(itemstack.getItem());
				float f2 = 0.3F;
				int i1 = block.getRenderType();

				if (i1 == 1 || i1 == 19 || i1 == 12 || i1 == 2)
					f2 = 0.5F;
				GL11.glPushMatrix();
//				if (Config.RotateItems)
//				{
					GL11.glRotatef((f1*10)%360, 0.0F, 1.0F, 0.0F);
//				}
				GL11.glScalef(f2, f2, f2);
				GL11.glTranslatef(0.0F, 0.35F, 0.0F);
				float f3 = 1.0F;
				blockrender.renderBlockAsItem(block, itemstack.getItemDamage(), f3);
				GL11.glPopMatrix();
			}
			else
			{
				GL11.glPushMatrix();
				GL11.glScalef(0.4F, 0.4F, 0.4F);

//				if (Config.RotateItems)
//				{
					GL11.glRotatef(180F - RenderManager.instance.playerViewY, 0.0F, 1.0F, 0.0F);
//				}

//				if (Config.Render3DItems)
//				{
					GL11.glTranslatef(-0.5F, 0.0F, 0.0F);
//				}

				if (itemstack.getItem().requiresMultipleRenderPasses())
				{
					for (int i = 0; i < itemstack.getItem().getRenderPasses(itemstack.getItemDamage()); i++)
					{
						IIcon j1 = itemstack.getItem().getIcon(itemstack, i);
						int l1 = itemstack.getItem().getColorFromItemStack(itemstack, i);
						float f5 = (float)(l1 >> 16 & 0xff) / 255F;
						float f7 = (float)(l1 >> 8 & 0xff) / 255F;
						float f9 = (float)(l1 & 0xff) / 255F;
						GL11.glColor4f(f5, f7, f9, 1.0F);
						drawItem(j1);
					}
				}
				else
				{
					IIcon l = itemstack.getIconIndex();

					int k1 = itemstack.getItem().getColorFromItemStack(itemstack, 0);
					float f4 = (float)(k1 >> 16 & 0xff) / 255F;
					float f6 = (float)(k1 >> 8 & 0xff) / 255F;
					float f8 = (float)(k1 & 0xff) / 255F;
					GL11.glColor4f(f4, f6, f8, 1.0F);
					drawItem(l);
				}

				GL11.glPopMatrix();
			}
		}
		GL11.glDisable(GL12.GL_RESCALE_NORMAL);
		GL11.glPopMatrix();
	}

	private void drawItem(IIcon icon)
	{
		if(icon == null)
			return;
		Tessellator tessellator = Tessellator.instance;
		float f = icon.getMinU();
		float f1 = icon.getMaxU();
		float f2 = icon.getMinV();
		float f3 = icon.getMaxV();

//		if (true)
//		{
			net.minecraft.client.renderer.ItemRenderer.renderItemIn2D(tessellator, f, f2, f1, f3, icon.getIconWidth(), icon.getIconHeight(), 0.0625F);
//		}
//		else
//		{
//			float f4 = 1.0F;
//			float f5 = 0.5F;
//			float f6 = 0.25F;
//			
//			tessellator.startDrawingQuads();
//			tessellator.setNormal(0.0F, 1.0F, 0.0F);
//			tessellator.addVertexWithUV(0.0F - f5, 0.0F - f6, 0.0D, f, f3);
//			tessellator.addVertexWithUV(f4 - f5, 0.0F - f6, 0.0D, f1, f3);
//			tessellator.addVertexWithUV(f4 - f5, 1.0F - f6, 0.0D, f1, f2);
//			tessellator.addVertexWithUV(0.0F - f5, 1.0F - f6, 0.0D, f, f2);
//			tessellator.addVertexWithUV(0.0F - f5, 1.0F - f6, 0.0D, f, f2);
//			tessellator.addVertexWithUV(f4 - f5, 1.0F - f6, 0.0D, f1, f2);
//			tessellator.addVertexWithUV(f4 - f5, 0.0F - f6, 0.0D, f1, f3);
//			tessellator.addVertexWithUV(0.0F - f5, 0.0F - f6, 0.0D, f, f3);
//			tessellator.draw();
//		}
	}
}
